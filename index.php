<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>s2-a1: Repetition Control Structures and Array Manipulation</title>
</head>
<body>
	<h2>Divisibles of Five</h2>
	<?php divisibleByFive(); ?>


	<h2>Array Manipulation</h2>
	
	<?php $students = []; ?>

	<?php array_push($students, "Joaquin Reyes"); ?>

	<p><?php var_dump($students); ?></p>

	<p><?= count($students); ?></p>

	<?php array_push($students, "Zoe Reyes"); ?>

	<p><?php var_dump($students); ?></p>

	<p><?= count($students); ?></p>	

	<?php array_shift($students); ?>

	<p><?php var_dump($students); ?></p>

	<p><?= count($students); ?></p>		


</body>
</html>